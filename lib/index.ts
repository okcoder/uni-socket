// 心跳包时间索引
let timer: any = null;
// 断线重连时间索引
let reConnectTimer: any = null;
// 是否登录
let isLogin: boolean = false;
// 当前网络状态
let netWork: boolean = true;
// 是否是主动退出
// let isClosed:boolean = false;
// 消息队列
let socketMsgQueue: [] = [];

interface Config {
    url: string,
    debug?: boolean,
    // 通用参数,只要在发送的数据为json才有效,优先率低
    params?: number | string | object,
    // 心跳检测开关
    isSendHeart?: boolean,
    // 断线重连开关
    isReconnection?: boolean,
    // 断线重连检测时间
    reConnectTime?: number,
    // 心跳检测频率
    timeout?: number, // 建议5分钟以内
    // 心跳数据
    heartData?: string | object,
    // 接收到消息
    onSocketMessage?: any,
    // socket被关闭回调
    onSocketClose?: any,
    // socket错误回调
    onSocketError?: any,
    // socket打开
    onSocketOpen?: any
}

export default class uniSocket {
    config: Config;
    initSocket: any;
    sendSocketMessage: any;
    closeSocket: any;

    constructor(config: Config) {
        this.config = {
            ...{
                url: "ws://127.0.0.1",
                params: {},
                debug: true,
                isSendHeart: true,
                isReconnection: true,
                reConnectTime: 3000,
                timeout: 4 * 60 * 1000,
                heartData: "ping",
                onSocketMessage: null,
                onSocketClose: null,
                onSocketError: null,
                onSocketOpen: null
            }, ...config
        };
        // 连接socket
        this.initSocket = (success: any, fail: any): any => {
            // isClosed = false;
            if (isLogin) {
                this.config.debug && console.log("%c [socket] %c 您已经登录了,请勿重新登录", 'color:#0f0;', 'color:#000;');
                return typeof success === "function" && success(this);
            }
            // 检查网络状态
            this.config.debug && console.log("%c [socket] %c 检查网络状态...", 'color:yellow;', 'color:#000;');
            // @ts-ignore
            uni.getNetworkType({
                fail: (err: any) => {
                    this.config.debug && console.log("%c [socket] %c 检查网络状态失败:", 'color:red;', 'color:#000;', err);
                    typeof fail === "function" && fail(err, this);
                },
                success: (res1: any) => {
                    if (res1.networkType === "none") {
                        this.config.debug && console.log("%c [socket] %c 网络已断开", 'color:red;', 'color:#000;');
                        isLogin = false;
                        netWork = false;
                        // @ts-ignore
                        uni.showModal({
                            title: "网络错误",
                            content: "请打开网络服务",
                            showCancel: false
                        });
                        typeof fail === "function" && fail(res1, this);
                    } else {
                        netWork = true;
                        this.config.debug && console.log("%c [socket] %c 网络正常,开始建立连接...", 'color:yellow;', 'color:#000;');
                        // @ts-ignore
                        uni.connectSocket({
                            url: this.config.url,
                            fail: (err: any) => {
                                this.config.debug && console.log("%c [socket] %c 连接socket失败:", 'color:red;',
                                    'color:#000;', err);
                                typeof fail === "function" && fail(err, this);
                            },
                            success: (res2: any) => {
                                console.log("%c [socket] %c 连接成功", 'color:blue;', 'color:#000;', res2)
                                typeof success === "function" && success(this);
                            }
                        });
                    }
                }
            });
        };

        // 发送socket消息
        this.sendSocketMessage = (data: string | number | object = "", success: any, fail: any): any => {
            if (typeof data === "object") {
                // @ts-ignore
                data = { ...this.config.params, ...data };
                this.config.debug && console.log("%c [socket] %c 发送消息", 'color:blue;', 'color:#000;', data);
                data = JSON.stringify(data);
            } else {
                this.config.debug && console.log("%c [socket] %c 发送消息", 'color:blue;', 'color:#000;', data);
            }
            if (!isLogin) {
                // @ts-ignore
                socketMsgQueue.push(data);
            } else {
                // @ts-ignore
                uni.sendSocketMessage({
                    data: data,
                    success,
                    fail
                });
            }
        }
        // 主动关闭socket
        this.closeSocket = (options: {
            code?: number,
            reason?: string,
            success?: undefined,
            fail?: undefined,
            complate?: undefined
        }): void => {
            this.config.debug && console.log("%c [socket] %c 主动关闭socket", 'color:red;', 'color:#000;');
            isLogin = false;
            // 主动退出
            // isClosed = true;
            if (this.config.isSendHeart) {
                this._clearHeart();
            }
            // @ts-ignore
            uni.closeSocket(options);
        };

        // 监听socket是否打开成功
        // @ts-ignore
        uni.onSocketOpen((header: object) => {
            this.config.debug && console.log("%c [socket] %c socket打开成功", 'color:blue;',
                'color:#000;', header);
            isLogin = true;
            // 判断是否需要发送心跳包
            if (this.config.isSendHeart) {
                this.config.debug && console.log("%c [socket] %c 检查到需要发送心跳包:", 'color:#0f0;',
                    'color:#000;', this.config.timeout);
                this._clearHeart();
                this._startHeart();
            }
            // 发送消息队列消息
            for (let i = 0; i < socketMsgQueue.length; i++) {
                this.config.debug && console.log("%c [socket] %c 正在发送消息队列消息:", 'color:blue;',
                    'color:#000;', i, socketMsgQueue[i]);
                this.sendSocketMessage(socketMsgQueue[i]);
            }
            socketMsgQueue = [];
            typeof this.config.onSocketOpen === "function" && this.config.onSocketOpen(this);
        });
        // @ts-ignore
        uni.onSocketMessage((data: any) => {
            let message = this._isJson(data.data) ? JSON.parse(data.data) : data.data;
            this.config.debug && console.log("%c [socket] %c 接收到消息:", 'color:blue;', 'color:#000;', message);
            typeof this.config.onSocketMessage === "function" && this.config.onSocketMessage(message);
        });
        // 监听网络状态
        // @ts-ignore
        uni.onNetworkStatusChange((res: any) => {
            this.config.debug && console.log("%c [socket] %c 监听到网络状态改变", 'color:#0f0;', 'color:#000;', JSON.stringify(
                res));
            if (res.isConnected) {
                if (!isLogin /*&& !isClosed */ && this.config.isReconnection) {
                    this.config.debug && console.log("%c [socket] %c 监听到有网络服务,重新连接socket", 'color:yellow;', 'color:#000;');
                    this._reConnectSocket();
                }
            } else {
                isLogin = false;
                this.config.isSendHeart && this._clearHeart();
                // @ts-ignore
                uni.showModal({
                    title: "网络错误",
                    content: "请打开网络服务",
                    showCancel: false
                });
            }
        });
        // 监听socket被关闭
        // @ts-ignore
        uni.onSocketClose((res: any) => {
            isLogin = false;
            typeof this.config.onSocketClose === "function" && this.config.onSocketClose(res);
            this.config.debug && console.log("%c [socket] %c 监听到socket被关闭了:", 'color:red;', 'color:#000;', JSON.stringify(
                res));
            // 停止心跳检查
            if (this.config.isSendHeart) {
                this._clearHeart();
            }
            if ( /*!isClosed && */ this.config.isReconnection) {
                // 断线重连
                this.config.debug && console.log("%c [socket] %c 非主动断开socket,重新连接中...", 'color:yellow;', 'color:#000;');
                this._reConnectSocket();
            }
        });
        // 监听socket错误
        // @ts-ignore
        uni.onSocketError((res: any) => {
            isLogin = false;
            this.config.debug && console.log("%c [socket] %c 监听到socket错误", 'color:red;', 'color:#000;', res);
            if(this.config.isReconnection){
              this._reConnectSocket();
            }
            typeof this.config.onSocketError === "function" && this.config.onSocketError(res);
        });
    }

    // socket重连
    _reConnectSocket() {
        if (isLogin) {
            this.config.debug && console.log("%c [socket] %c 在登录状态,无法重连", 'color:red;', 'color:#000;');
        } else {
            reConnectTimer = setInterval(() => {
                this.initSocket(function (e: any) {
                    e.config.debug && console.log("%c [socket] %c 重新连接成功", 'color:yellow;', 'color:#000;');
                    if (e.config.isSendHeart) {
                        e._clearHeart();
                        e._startHeart();
                    }
                    clearInterval(reConnectTimer);
                    reConnectTimer = null;
                }, function (err: any, e: any) {
                    e.config.debug && console.log("%c [socket] %c 重新连接失败", 'color:red;', 'color:#000;');
                });
            }, this.config.reConnectTime);
        }
    }

    // 清除心跳
    _clearHeart(): void {
        this.config.debug && console.log("%c [socket] %c 已清除心跳", 'color:#0f0;', 'color:#000;');
        clearInterval(timer);
    }

    // 心跳开始
    _startHeart(): void {
        this.config.debug && console.log("%c [socket] %c 心跳开始", 'color:#0f0;', 'color:#000;')
        timer = setInterval(() => {
            console.info("%c [socket] %c 登录状态", 'color:#0f0;', 'color:#000;', isLogin);
            this.sendSocketMessage(this.config.heartData);
        }, this.config.timeout);
    }

    // 是否是json字符串
    _isJson(str: string): boolean {
        try {
            var obj = JSON.parse(str);
            return !!(typeof obj == 'object' && obj);
        } catch (e) {
            return false;
        }
    }
}